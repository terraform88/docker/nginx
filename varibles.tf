variable "container_name" {
  description = "Define container name"
  type        = string
  default     = "nginx"
}

variable "internal_port" {
  description = "Port forward"
  default     = "80"
}

variable "external_port" {
  description = "Port in container"
  default     = "8080"
}